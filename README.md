### Differents requettes de l'api

@PostMapping(path = "/auth",consumes = "application/json", produces = "application/json")

@PostMapping(path = "/api/addUser",consumes = "application/json", produces = "application/json")

@GetMapping(path = "/api/user/{id}",consumes = "application/json", produces = "application/json")

@PutMapping(path = "/api/update/user/{id}",consumes = "application/json", produces = "application/json")

@GetMapping(path = "/api/events")

@GetMapping(path = "/api/event/{eid}")

@GetMapping(path = "/api/event/byAuthor/{uid}")

@GetMapping(path = "/api/events/byParticipant/{uid}")

@PostMapping(path = "/api/add/event",consumes = "application/json", produces = "application/json")

@DeleteMapping(path = "/api/delete/event/{id}",consumes = "application/json", produces = "application/json")

@PostMapping(path = "/api/event/participate/{uid}/{eid}",consumes = "application/json", produces = "application/json")

@PostMapping(path = "/api/event/removeParticipation/{uid}/{eid}",consumes = "application/json", produces = "application/json")

@GetMapping(path = "/api/sports")
### Front end du projet

 [Front end du projet en angular](https://gitlab.com/fbenrabh/frontsportpartn).
