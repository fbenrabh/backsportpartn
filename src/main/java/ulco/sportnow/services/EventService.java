package ulco.sportnow.services;

import org.springframework.data.crossstore.ChangeSetPersister;
import ulco.sportnow.app.JwtUtil;
import ulco.sportnow.dao.UserRepository;
import ulco.sportnow.entities.Event;
import ulco.sportnow.entities.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ulco.sportnow.dao.EventRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class EventService {
    @Autowired
	private EventRepository eventRepository;
	@Autowired
	private UserRepository userRepository;

    public Event addEvent(Event event,User author) { ;
		author = userRepository.findUserByEmail(author.getEmail());
		//System.out.println(author.getEmail());
		event.setAuthor(author);
		if(event.getParticipants()==null)
			event.setParticipants(new ArrayList<User>());
		if(!event.getParticipants().contains(author))
			event.getParticipants().add(author);
        event.setCreationDate(new Date());
    	return  eventRepository.save(event);
    }

	public List<Event> getEvents() {
		// TODO Auto-generated method stub
		return eventRepository.findAll();
	}
	public Event getEvent(String id) {
    	return eventRepository.findEventById(id);
	}

	public Event participateToEvent(String userId, String eventId) {
    	Event event = eventRepository.findEventById(eventId);
    	User user = userRepository.findUserById(userId);
    	if(!event.getParticipants().contains(user))
    	event.getParticipants().add(user);
    	eventRepository.save(event);
		return event;
	}

	public Event removeParticipation(String userId, String eventId) {
		Event event = eventRepository.findEventById(eventId);
		User user = userRepository.findUserById(userId);
		if(event.getParticipants().contains(user))
			event.getParticipants().remove(user);
		return eventRepository.save(event);
	}

	public Event removeEvent(String eventId, User author) throws Exception {
		author = userRepository.findUserByEmail(author.getEmail());
		Event event = eventRepository.findEventById(eventId);
		if(event.getAuthor().getEmail().equals(author.getEmail()) && event.getAuthor().getId().equals(author.getId())){
			eventRepository.delete(event);
			return event;
		}
        throw new Exception("you dont have permission to delete this event !");
	}

	public List<Event> getEventsByAuthor(String id) {
		User author = userRepository.findUserById(id);
		return eventRepository.findEventByAuthor(author);
	}

	public List<Event> getEventsByParticipant(String id) {
		User u = userRepository.findUserById(id);
		return eventRepository.findEventByParticipantsContains(u);
	}
}
