package ulco.sportnow.services;

import ulco.sportnow.entities.User;
import ulco.sportnow.dao.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService implements  UserDetailsService {
    @Autowired
    private UserRepository userRepository;


    public User addUser(User user){
        User u = userRepository.findUserByEmail(user.getEmail());
        if(u == null){
            user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
            user.setImgUrl("https://i.imgur.com/04SjkW8.png");
            return userRepository.save(user);
        }
        return u;
    }


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
       return userRepository.findUserByEmail(email);

    }


    public User getUser(String uid) {
        return userRepository.findUserById(uid);
    }

    public User updateUser(String uid,User update) {
        User u= userRepository.findUserById(uid);
        u.setAboutMe(update.getAboutMe());
        u.setImgUrl(update.getImgUrl());
        u.setMySports(update.getMySports());
        u.setName(update.getName());
        return userRepository.save(u);
    }
}
