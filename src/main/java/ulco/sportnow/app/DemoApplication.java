package ulco.sportnow.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages= "ulco.sportnow")
@EnableJpaRepositories("ulco.sportnow.dao")
@EntityScan("ulco.sportnow.entities")
//@ComponentScan(basePackages= "ulco.sportnow.app")
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
