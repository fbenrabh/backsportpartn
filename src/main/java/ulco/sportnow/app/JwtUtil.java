package ulco.sportnow.app;

import ulco.sportnow.entities.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class JwtUtil {

    private String secret="secret";

    public User parseToken(String token) {
        try {
            Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();

            User u = new User();
            u.setEmail(body.getSubject());
            u.setId(body.getId());
            //u.setRole((String) body.get("role"));
            return u;
        } catch (JwtException | ClassCastException e) {
            return null;
        }
    }


    public String generateToken(User u) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("fullName",u.getName());
        return Jwts.builder().setClaims(claims).setSubject(u.getEmail()).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000*60*60 *5))
                .setId(u.getId())
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, secret).compact();
    }

    public boolean isTokenExpired(String token){
            Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
            return body.getExpiration().before(new Date());
    }

    public boolean validateToken(String token , User user){
        final User tokenUser = parseToken(token);
        return (tokenUser.getEmail().equals(user.getEmail()) && !isTokenExpired(token));
    }
}
