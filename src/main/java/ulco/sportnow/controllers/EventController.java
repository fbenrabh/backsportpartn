package ulco.sportnow.controllers;

import org.springframework.http.ResponseEntity;
import ulco.sportnow.app.JwtUtil;
import ulco.sportnow.entities.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ulco.sportnow.entities.Sport;
import ulco.sportnow.entities.User;
import ulco.sportnow.services.EventService;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class EventController {
	@Autowired
    private EventService eventService;
	@Autowired
	private JwtUtil jwtUtil;

	@GetMapping(path = "/api/events")
	public List<Event> getEvents() {
		List<Event> e = eventService.getEvents();
		return e;
	}

	@GetMapping(path = "/api/event/{eid}")
	public Event getEvent(@PathVariable("eid") String eventId) {
		return eventService.getEvent(eventId);
	}

	@GetMapping(path = "/api/event/byAuthor/{uid}")
	public List<Event> getEventsByAuthor(@PathVariable("uid") String id) {
		return eventService.getEventsByAuthor(id);
	}

	@GetMapping(path = "/api/events/byParticipant/{uid}")
	public List<Event> getEventsByParticipant(@PathVariable("uid") String id) {
		return eventService.getEventsByParticipant(id);
	}

	@PostMapping(path = "/api/add/event",consumes = "application/json", produces = "application/json")
	public Event addEvent(@RequestBody Event event, @RequestHeader (name="Authorization") String token) {
        User author = jwtUtil.parseToken(token.substring(7));
        //System.out.println(author.getEmail());
		return  eventService.addEvent(event,author);
	}

	@DeleteMapping(path = "/api/delete/event/{id}",consumes = "application/json", produces = "application/json")
	public ResponseEntity<String> removeEvent(@PathVariable(value = "id") String eventId, @RequestHeader (name="Authorization") String token) {
		User author = jwtUtil.parseToken(token.substring(7));
		try {
			 eventService.removeEvent(eventId,author);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().build();
		}
		return  ResponseEntity.ok("event deleted !");
	}


	@PostMapping(path = "/api/event/participate/{uid}/{eid}",consumes = "application/json", produces = "application/json")
	public Event participate(@PathVariable(value = "uid") String userId ,@PathVariable(value = "eid") String eventId ) {
		return  eventService.participateToEvent(userId,eventId);
	}

	@PostMapping(path = "/api/event/removeParticipation/{uid}/{eid}",consumes = "application/json", produces = "application/json")
	public Event removeParticipation(@PathVariable(value = "uid") String userId ,@PathVariable(value = "eid") String eventId ) {
		return  eventService.removeParticipation(userId,eventId);
	}

	@GetMapping(path = "/api/sports")
	public Sport[] getSports(){
		return Sport.values();
	}

}
