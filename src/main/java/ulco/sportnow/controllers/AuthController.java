package ulco.sportnow.controllers;

import org.springframework.web.bind.annotation.*;
import ulco.sportnow.entities.AuthResponse;
import ulco.sportnow.entities.User;
import ulco.sportnow.app.JwtUtil;
import ulco.sportnow.entities.AuthRequest;
import ulco.sportnow.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

@RestController
@CrossOrigin(origins = "*")
public class AuthController {
    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private UserService userService;
    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping(path = "/auth",consumes = "application/json", produces = "application/json")
    public AuthResponse login(@RequestBody AuthRequest authRequest) throws Exception{
        try {
           authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getEmail(),authRequest.getPassword()));
        }catch (BadCredentialsException e){
             throw new Exception("invalide email or password !");
        }
        User u = (User) userService.loadUserByUsername(authRequest.getEmail());
        String token =jwtUtil.generateToken(u);
        return new AuthResponse(token,u.getId(),u.getName());
    }

    @GetMapping(path="/")
    public String home(){
        return "SportNow API";
    }

}
