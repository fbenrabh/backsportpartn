package ulco.sportnow.controllers;

import org.springframework.web.bind.annotation.*;
import ulco.sportnow.entities.User;
import ulco.sportnow.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
@CrossOrigin(origins = "*")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping(path = "/api/addUser",consumes = "application/json", produces = "application/json")
    public User addUser(@RequestBody User user) {
        return  userService.addUser(user);
    }

    @GetMapping(path = "/api/user/{id}",consumes = "application/json", produces = "application/json")
    public User getUser(@PathVariable("id") String uid) {
        return  userService.getUser(uid);
    }

    @PutMapping(path = "/api/update/user/{id}",consumes = "application/json", produces = "application/json")
    public User updateUser(@PathVariable("id") String uid ,@RequestBody User update ) {
        return  userService.updateUser(uid,update);
    }
}
