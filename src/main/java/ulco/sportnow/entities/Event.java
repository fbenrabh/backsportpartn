package ulco.sportnow.entities;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.ManyToAny;


import javax.persistence.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Event {
    @Id @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
	private String id;
    private String name;
    private Date creationDate;
    private Date eventDate;
    private String description;
    private String eventAddress;
    private String city;
    private String postalCode;
    private int nbParticipants;
    private String sport;
    private boolean isOpen;
    private boolean isPrivate;
    @ManyToOne
    private User author;
    @ManyToMany
    private List<User> participants ;
}
