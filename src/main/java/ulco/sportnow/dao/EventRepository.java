package ulco.sportnow.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ulco.sportnow.entities.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ulco.sportnow.entities.User;

import java.util.List;

@Repository
public interface EventRepository  extends JpaRepository<Event,String>{
  Event findEventById(String id);

  List<Event> findEventByAuthor(User author);

  List<Event> findEventByParticipantsContains(  User u);
}
